package com.bk.filmfan.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.Slide;
import com.bk.filmfan.repositories.MovieRepository;

import java.util.List;

public class MovieListViewModel extends ViewModel {

     private MovieRepository movieRepository;

    //constructor
    public MovieListViewModel() {
        movieRepository= MovieRepository.getInstance();
    }
    public LiveData<List<Movie>> getMovies(){
       return movieRepository.getMovies();
    }
    public LiveData<List<Movie>> getSlides(){return movieRepository.getSlides();}
    public LiveData<List<Cast>> getCasts(){
        return movieRepository.getCasts();
    }

    // 3- calling get now playing api inside view model
    public void getNowPlayingMoviesApi(int pageNumber){
        movieRepository.getNowPlayingMoviesApi(pageNumber);
    }

    public void getMovieCastsApi(int movie_id){
        movieRepository.getMovieCastsApi(movie_id);
    }
}
