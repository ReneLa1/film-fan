package com.bk.filmfan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bk.filmfan.adapter.CastAdapter;
import com.bk.filmfan.adapter.CastClickListener;
import com.bk.filmfan.adapter.MovieAdapter;
import com.bk.filmfan.adapter.MovieClickListener;
import com.bk.filmfan.helpers.Helpers;
import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.MovieRate;
import com.bk.filmfan.request.CallService;
import com.bk.filmfan.request.MovieApi;
import com.bk.filmfan.viewmodels.MovieListViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailActivity extends AppCompatActivity implements CastClickListener {

    private ImageButton imageButton;
    private ImageView moviePosterImg,coverImg;
    private TextView mv_title, mv_desc,mv_release_year,mv_rating, ratingValue;

    private RecyclerView castRV;
    private CastAdapter castRecyclerAdapter;

    private MovieListViewModel movieListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        //in views
        inViews();

        // cast recyclerview setup
        ObserveMovieCastsChange();
        ConfigureRvCast();


        MovieRate mRate = new MovieRate(8);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallRetrofitRateMovie(550,mRate);
            }
        });
    }

    void inViews(){

        castRV= findViewById(R.id.cast_rv);
        moviePosterImg= findViewById(R.id.movie_detail_poster);
        coverImg= findViewById(R.id.detail_poster);
        mv_title=findViewById(R.id.details_title);
        mv_desc= findViewById(R.id.movie_desc);
        mv_release_year= findViewById(R.id.release_year_value);
        mv_rating= findViewById(R.id.rate_value);
        imageButton=findViewById(R.id.vote_imageButton);
        GetDataFromIntent();
    }

    public void GetDataFromIntent(){
      if(getIntent().hasExtra("movie")){
          Movie movie = getIntent().getParcelableExtra("movie");
          getMovieCastsApi(movie.getMovie_id());
          mv_title.setText(movie.getTitle());
          mv_desc.setText(movie.getOverview());
          mv_release_year.setText(movie.getRelease_date());
          mv_rating.setText(movie.getAverage_vote()+"/10");

          Glide.with(this)
                  .load("https://image.tmdb.org/t/p/w500/"
                          +movie.getPoster_path())
                  .into(coverImg);
          Glide.with(this)
                  .load("https://image.tmdb.org/t/p/w500/"
                          +movie.getPoster_path())
                  .into(moviePosterImg);
          Log.v("Tag"," Incoming data: "+movie.getMovie_id());
      }
    }

    //Observing for casts data changes
    private void ObserveMovieCastsChange(){
        movieListViewModel.getCasts().observe(this, new Observer<List<Cast>>() {
            @Override
            public void onChanged(List<Cast> casts) {
                if(casts!=null){
                    for(Cast cast:casts){
                        //Get data in the log
                        Log.v("Tag", "Actor profile " + cast.getProfile_path());
                       castRecyclerAdapter.setmCasts((casts));
                    }
                }
            }
        });
    }

    // initializing cast recyclerview and adding data to it
    void ConfigureRvCast(){
        castRecyclerAdapter= new CastAdapter();
        castRV.setAdapter(castRecyclerAdapter);
        castRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));

    }

    private void getMovieCastsApi(int movie_id) {
        movieListViewModel.getMovieCastsApi(movie_id);
    }

    @Override
    public void onCastClick(Cast cast) {
    }

    private void CallRetrofitRateMovie(int movie_id, MovieRate movieRate){
        MovieApi movieApi = CallService.getMovieApi();

        Call<MovieRate> responseCall= movieApi.rateMovie(movie_id, Helpers.API_KEY,movieRate);

        responseCall.enqueue(new Callback<MovieRate>() {
            @Override
            public void onResponse(Call<MovieRate> call, Response<MovieRate> response) {

                //display results
               mv_rating.setText( String.valueOf(response.body()));
            }
            @Override
            public void onFailure(Call<MovieRate> call, Throwable t) {

            }
        });
    }
}