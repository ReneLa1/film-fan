package com.bk.filmfan.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Slide implements Parcelable {
    int id;
    private String title;
    private String release_date;
    private float vote_average;
    private String poster_path;
    private String overview;
    private int Image ;
    private String Title;



    public Slide(int id, String title, String release_date, float vote_average, String poster_path, String overview) {
        this.id = id;
        this.title = title;
        this.release_date = release_date;
        this.vote_average = vote_average;
        this.poster_path = poster_path;
        this.overview = overview;

    }

    public Slide(int image, String title) {
        Image = image;
        Title = title;
    }

    protected Slide(Parcel in) {
        id = in.readInt();
        title = in.readString();
        release_date = in.readString();
        vote_average = in.readFloat();
        poster_path = in.readString();
        overview = in.readString();
        Image = in.readInt();
        Title = in.readString();
    }

    public static final Creator<Slide> CREATOR = new Creator<Slide>() {
        @Override
        public Slide createFromParcel(Parcel in) {
            return new Slide(in);
        }

        @Override
        public Slide[] newArray(int size) {
            return new Slide[size];
        }
    };

    public int get_id() {
        return id;
    }

    public void setMovie_id(int id) {
        this.id = id;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public int getImage() {
        return Image;
    }

    public String getTitle() {
        return Title;
    }

    public void setImage(int image) {
        Image = image;
    }

    public void setTitle(String title) {
        Title = title;
    }

    @Override
    public String toString() {
        return "Slide{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", release_date='" + release_date + '\'' +
                ", vote_average=" + vote_average +
                ", poster_path='" + poster_path + '\'' +
                ", overview='" + overview + '\'' +
                ", Image=" + Image +
                ", Title='" + Title + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(release_date);
        parcel.writeFloat(vote_average);
        parcel.writeString(poster_path);
        parcel.writeString(overview);
        parcel.writeInt(Image);
        parcel.writeString(Title);
    }
}
