package com.bk.filmfan.models;

public class MovieRate {

    private float value;

    public MovieRate(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MovieRate{" +
                "value=" + value +
                '}';
    }
}
