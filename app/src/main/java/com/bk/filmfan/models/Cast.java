package com.bk.filmfan.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Cast implements Parcelable {

    String name;
    String character;
    String profile_path;
    int photo;

    public Cast(String name, String character, String profile_path) {
        this.name = name;
        this.character = character;
        this.profile_path = profile_path;
    }

    public Cast(String name, int photo) {
        this.name = name;
        this.photo = photo;
    }

    protected Cast(Parcel in) {
        name = in.readString();
        character = in.readString();
        profile_path = in.readString();
        photo = in.readInt();
    }

    public static final Creator<Cast> CREATOR = new Creator<Cast>() {
        @Override
        public Cast createFromParcel(Parcel in) {
            return new Cast(in);
        }

        @Override
        public Cast[] newArray(int size) {
            return new Cast[size];
        }
    };

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Cast{" +
                "name='" + name + '\'' +
                ", character='" + character + '\'' +
                ", profile_path='" + profile_path + '\'' +
                ", photo=" + photo +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(character);
        parcel.writeString(profile_path);
        parcel.writeInt(photo);
    }
}
