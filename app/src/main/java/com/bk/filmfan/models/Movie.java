package com.bk.filmfan.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;
import java.util.List;

public class Movie implements Parcelable, Comparable<Movie>{
    private int id;
    private String title;
    private String release_date;
    private float vote_average;
    private String poster_path;
    private String overview;
    private List<Genre> genres;
    private int image;
    private int coverPhoto;


    public Movie(int id, String title, String release_date, float vote_average, String poster_path, String overview, List<Genre> genres) {
        this.id = id;
        this.title = title;
        this.release_date = release_date;
        this.vote_average = vote_average;
        this.poster_path = poster_path;
        this.overview = overview;
        this.genres = genres;
    }

    public Movie(String title, int image) {
        this.title = title;
        this.image = image;
    }

    public Movie(String title, String release_date, float vote_average, int image) {
        this.title = title;
        this.release_date = release_date;
        this.vote_average = vote_average;
        this.image = image;
    }

    protected Movie(Parcel in) {
        id = in.readInt();
        title = in.readString();
        release_date = in.readString();
        vote_average = in.readFloat();
        poster_path = in.readString();
        overview = in.readString();
        image = in.readInt();
        coverPhoto = in.readInt();
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(release_date);
        dest.writeFloat(vote_average);
        dest.writeString(poster_path);
        dest.writeString(overview);
        dest.writeInt(image);
        dest.writeInt(coverPhoto);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public int getMovie_id() {
        return id;
    }

    public void setMovie_id(int movie_id) {
        this.id = movie_id;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public int getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(int coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public float getAverage_vote() {
        return vote_average;
    }

    public void setAverage_vote(float vote_average) {
        this.vote_average = vote_average;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", release_date='" + release_date + '\'' +
                ", vote_average=" + vote_average +
                ", poster_path='" + poster_path + '\'' +
                ", overview='" + overview + '\'' +
                ", genres=" + genres +
                ", image=" + image +
                ", coverPhoto=" + coverPhoto +
                '}';
    }

    @Override
    public int compareTo(Movie movie) {
        return this.id - movie.getMovie_id() ;
    }
}
