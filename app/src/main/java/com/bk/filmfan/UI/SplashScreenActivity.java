package com.bk.filmfan.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.bk.filmfan.HomeActivity;
import com.bk.filmfan.R;

public class SplashScreenActivity extends AppCompatActivity {

    
    Thread timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        
        timer =new Thread(){
            @Override
            public void  run(){
                try{
                    synchronized (this){
                        wait(3000);
                    }
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    Intent intent = new Intent(SplashScreenActivity.this, WelcomeScreenActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        };
        timer.start();
    }
}