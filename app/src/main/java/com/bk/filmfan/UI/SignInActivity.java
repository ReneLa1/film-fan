package com.bk.filmfan.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bk.filmfan.HomeActivity;
import com.bk.filmfan.R;

public class SignInActivity extends AppCompatActivity {
    Button loginBtn;
    TextView signUpClickable;
    EditText emailInput, passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        inViews();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(SignInActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

//        signUpClickable.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent= new Intent(SignInActivity.this, SignUpActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    void inViews(){
        loginBtn = findViewById(R.id.login_btn);
        signUpClickable = findViewById(R.id.sign_clickable_text);

    }

}