package com.bk.filmfan.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bk.filmfan.R;

public class WelcomeScreenActivity extends AppCompatActivity {
    Button loginBtn, registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        inViews();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeScreenActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

//        registerBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(WelcomeScreenActivity.this, SignUpActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    void inViews(){
        loginBtn= findViewById(R.id.sign_in_btn);
        registerBtn = findViewById(R.id.sign_up_btn);
    }

    //navigate to login screen


}