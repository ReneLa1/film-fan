package com.bk.filmfan.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.Slide;
import com.bk.filmfan.request.MovieApiClient;

import java.util.List;

//this class acts as repository
public class MovieRepository {

    //singleton pattern
    private static MovieRepository instance;

    private MovieApiClient movieApiClient;

    public static  MovieRepository getInstance(){
     if(instance==null){
         instance=new MovieRepository();
     }
     return instance;
    };

    private MovieRepository(){
        movieApiClient = MovieApiClient.getInstance();
    }

    public MovieApiClient getMovieApiClient() {
        return movieApiClient;
    }

    public LiveData<List<Movie>> getMovies(){return movieApiClient.getMovies();};

    public LiveData<List<Cast>> getCasts(){return movieApiClient.getCasts();};

    public LiveData<List<Movie>> getSlides(){return movieApiClient.getSlides();};

    // 2 - calling the now playing api inside repository
    public  void getNowPlayingMoviesApi(int pageNumber){
        movieApiClient.getNowPlayingMoviesApi(pageNumber);
    }

    public  void getMovieCastsApi(int movie_id){
        movieApiClient.getMovieCasts(movie_id);
    }
}
