package com.bk.filmfan.request;

import com.bk.filmfan.helpers.Helpers;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CallService {
    private static Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl(Helpers.ROOT_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static  Retrofit retrofit = retrofitBuilder.build();

    private static MovieApi movieApi= retrofit.create(MovieApi.class);

    public static MovieApi getMovieApi(){
        return movieApi;
    }
}
