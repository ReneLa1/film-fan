package com.bk.filmfan.request;

import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.MovieRate;
import com.bk.filmfan.response.MovieCastsResponse;
import com.bk.filmfan.response.MovieResponse;
import com.bk.filmfan.response.NowPlayingResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApi {

    //get now playing movies
    @GET("/3/movie/now_playing")
    Call<NowPlayingResponse> getNowPlayingMovies(
            @Query("api_key") String key,
            @Query("page")  int pageNumber
    );

    //get movie by ID
    @GET("3/movie/{movie_id}?")
    Call<Movie> getMovie(
      @Path("movie_id") int movie_id,
      @Query("api_key") String api_key
    );

    //get movie casts by movie id
    @GET("3/movie/{movie_id}/credits?")
    Call<MovieCastsResponse> getMovieCasts(@Path("movie_id") int movie_id,
                                           @Query("api_key") String api_key);

    //Rate movie
    @POST("movie/{movie_id}/rating")
    @Headers({"Content-Type: application/json;charset=utf-8"})
    Call<MovieRate> rateMovie(@Path("movie_id") int movie_id,
                              @Query("api_key") String api_key,
                              @Body MovieRate movieRate);
}
