package com.bk.filmfan.request;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bk.filmfan.AppExecutors;
import com.bk.filmfan.helpers.Helpers;
import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.MovieRate;
import com.bk.filmfan.models.Slide;
import com.bk.filmfan.response.MovieCastsResponse;
import com.bk.filmfan.response.NowPlayingResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

//this class acts as a bridge between retrofit api and live data
public class MovieApiClient {

    //Live Data
    private MutableLiveData<List<Movie>> mMovies;
    private MutableLiveData<List<Movie>> mSlides;
    private MutableLiveData<List<Cast>> mCasts;


    //making a global runnable request
    private RetrieveNowPlayingRunnable retrieveNowPlayingRunnable;

    private RetrieveMovieCastRunnable retrieveMovieCastRunnable;

    private static MovieApiClient instance;

    public static MovieApiClient getInstance(){
        if(instance==null){
            instance=new MovieApiClient();
        }
        return instance;
    }

    //create live data
    private MovieApiClient(){
        mMovies= new MutableLiveData<>();
        mCasts= new MutableLiveData<>();
        mSlides= new MutableLiveData<>();
      
    }

    public LiveData<List<Movie>> getMovies(){
        return mMovies;
    }
    public LiveData<List<Cast>> getCasts(){return mCasts;};
    public LiveData<List<Movie>> getSlides(){return mSlides;};


    //1 - this is method is to be called through the classes
    public void getNowPlayingMoviesApi(int pageNumber){

        if(retrieveNowPlayingRunnable!= null){
            retrieveNowPlayingRunnable=null;
        }

        retrieveNowPlayingRunnable =  new RetrieveNowPlayingRunnable(pageNumber);
        final Future myHandler = AppExecutors.getInstance().networkIO().submit(retrieveNowPlayingRunnable);
        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //cancelling the retrofit call
            myHandler.cancel(true);
            }
        },4000, TimeUnit.MILLISECONDS);

}

    public void getMovieCasts(int movie_id){
        if(retrieveMovieCastRunnable!= null){
            retrieveMovieCastRunnable=null;
        }
        retrieveMovieCastRunnable=  new RetrieveMovieCastRunnable(movie_id);
        final Future myHandler2 = AppExecutors.getInstance().networkIO().submit(retrieveMovieCastRunnable);
        AppExecutors.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //cancelling the retrofit call
                myHandler2.cancel(true);
            }
        },4000, TimeUnit.MILLISECONDS);
    }

    //retrieve data from RestApi and store to live data by runnable class
    private class RetrieveNowPlayingRunnable implements Runnable{

        private int pageNumber;
        boolean cancelRequest;

        public RetrieveNowPlayingRunnable(int pageNumber) {
            this.pageNumber = pageNumber;
            cancelRequest =false;
        }

        @Override
        public void run() {
            //get response objects
            try{
                Response response1 = getMovies(pageNumber).execute();
                if(cancelRequest){
                    return;
                }
                if(response1.code()==200){
                    List<Movie> list = new ArrayList<>(((NowPlayingResponse) response1.body()).getMovies());
                    if(pageNumber== 1){
                        //sending data to live data
                        //PostValue: Used for background thread
                        //setValue: not for background thread
                       mMovies.postValue(list);
                       mSlides.postValue(list);

                    }else{
                        List<Movie> currentMovies = mMovies.getValue();
                        List<Movie> currentSlides = mSlides.getValue();

                        currentMovies.addAll(list);
                        currentSlides.addAll(list);

                        mMovies.postValue(currentMovies);
                        mSlides.postValue(currentSlides);
                    }
                }else{
                    String error = response1.errorBody().string();
                    Log.v("Tag","Error:  "+error);
                    mMovies.postValue(null);
                    mSlides.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(cancelRequest){
                return;
            }
        }
            //get now play movies
            private Call<NowPlayingResponse> getMovies(int pageNumber){
                return CallService.getMovieApi().getNowPlayingMovies(Helpers.API_KEY,pageNumber);

            }
            private void cancelRequest(){
                Log.v("Tag","Cancelling request for retrieving now playing movies");
                cancelRequest=true;
            }
    }

    private class RetrieveMovieCastRunnable implements Runnable{
        private int movie_id;
        boolean cancelRequest;

        public RetrieveMovieCastRunnable(int movie_id) {
            this.movie_id=movie_id;
            cancelRequest =false;
        }

        @Override
        public void run() {
            //get response objects
            try{
                Response response2 = getCasts().execute();
                if(cancelRequest){
                    return;
                }
                if(response2.code()==200){
                    List<Cast> list = new ArrayList<>(((MovieCastsResponse) response2.body()).getCasts());
                    mCasts.postValue(list);

                }else{
                    String error = response2.errorBody().string();
                    Log.v("Tag","Error:  "+error);
                    mMovies.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(cancelRequest){
                return;
            }
        }

        //get movie casts
        private Call<MovieCastsResponse> getCasts(){
            return CallService.getMovieApi().getMovieCasts(movie_id,Helpers.API_KEY);
        }

        private void cancelRequest(){
            Log.v("Tag","Cancelling request for retrieving movie casts");
            cancelRequest=true;
        }



    }

}




