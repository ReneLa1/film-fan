package com.bk.filmfan.adapter;


import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;

public interface CastClickListener {
    void onCastClick(Cast cast);
}
