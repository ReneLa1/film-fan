package com.bk.filmfan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bk.filmfan.R;
import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.zip.Inflater;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    private Context context;
    private List<Movie> mMovies;

    private MovieClickListener movieClickListener;


    public MovieAdapter(MovieClickListener movieClickListener) {
        this.movieClickListener = movieClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mvTitle.setText(mMovies.get(position).getTitle());
        holder.releaseY .setText(mMovies.get(position).getRelease_date());
        holder.ratingBar.setRating((mMovies.get(position).getVote_average())/2);

        //Imageview: using Glide library
        Glide.with(holder.itemView.getContext())
                .load( "https://image.tmdb.org/t/p/w500/"
                        +mMovies.get(position).getPoster_path())
                .into((holder).mvImage);

    }

    @Override
    public int getItemCount() {
        if(mMovies!=null){
            return mMovies.size();
        }
        return 0;
    }

    public void setmMovies(List<Movie> mMovies) {
        this.mMovies = mMovies;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mvTitle, releaseY;
        private ImageView mvImage;
        RatingBar ratingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mvTitle= itemView.findViewById(R.id.movie_title);
            mvImage =itemView.findViewById(R.id.movie_poster);
            releaseY= itemView.findViewById(R.id.release_y);
            ratingBar= itemView.findViewById(R.id.movie_rating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    movieClickListener.onMovieClick(mMovies.get(getAdapterPosition()),mvImage);
                }
            });
        }
    }
}
