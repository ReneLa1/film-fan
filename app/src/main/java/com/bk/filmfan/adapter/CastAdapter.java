package com.bk.filmfan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bk.filmfan.R;
import com.bk.filmfan.models.Cast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {

    Context mContext;
    List<Cast> mCasts;

    private CastClickListener castClickListener;


    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cast_item, parent, false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, int position) {
        holder.castName.setText(mCasts.get(position).getName());
        holder.castChar.setText("( "+mCasts.get(position).getCharacter()+" )");

        //Imageview: using Glide library
        Glide.with(holder.itemView.getContext())
                .load( "https://image.tmdb.org/t/p/w500"
                        +mCasts.get(position).getProfile_path())
                .apply(new RequestOptions().circleCrop())
                .into((holder).castProfile);
    }

    @Override
    public int getItemCount() {
        if(mCasts!=null){
            return mCasts.size();
        }
        return 0;
    }

    public void setmCasts(List<Cast>mCasts) {
        this.mCasts = mCasts;
        notifyDataSetChanged();
    }

    public class  CastViewHolder extends RecyclerView.ViewHolder {

        private ImageView castProfile;
        private TextView castName, castChar;

        public CastViewHolder(@NonNull View itemView) {
            super(itemView);
            castProfile= itemView.findViewById(R.id.cast_profile);
            castName= itemView.findViewById(R.id.cast_name);
            castChar = itemView.findViewById(R.id.cast_char);

        }
    }
}


