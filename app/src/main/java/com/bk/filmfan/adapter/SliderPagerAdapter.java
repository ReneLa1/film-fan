package com.bk.filmfan.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bk.filmfan.R;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.Slide;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {

    private Context mContext ;
    private List<Movie> mMovies ;
    private MovieClickListener slideClickListener;

    public SliderPagerAdapter(MovieClickListener movieClickListener) {
        this.slideClickListener = movieClickListener;
    }

    public SliderPagerAdapter(Context mContext, List<Movie> mMovies) {
        this.mContext = mContext;
        this.mMovies = mMovies;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View slideLayout = inflater.inflate(R.layout.slider_poster_item,null);

        FloatingActionButton sliderPlayBtn;

        sliderPlayBtn = slideLayout.findViewById(R.id.slider_player_btn);
        ImageView slideImg = slideLayout.findViewById(R.id.slider_poster_img);
        TextView slideText = slideLayout.findViewById(R.id.slider_poster_title);
        RatingBar rating_mv =slideLayout.findViewById(R.id.slider_ratingBar);
//        slideImg.setImageResource(mSlides.get(position).getImage());

        try {
            Glide.with(container.getContext())
                    .load( "https://image.tmdb.org/t/p/w500/"
                            +mMovies.get(position).getPoster_path())
                    .into(slideImg);
        } catch (NumberFormatException nfe) {
            // Handle the condition when str is not a number.
            Log.i("Tag", "" + nfe);
        }

        slideText.setText(mMovies.get(position).getTitle());
        rating_mv.setRating((mMovies.get(position).getVote_average())/2);

        container.addView(slideLayout);
        sliderPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slideClickListener.onSlideClick(mMovies.get(position),slideImg);
            }
        });
        return slideLayout;

    }

    @Override
    public int getCount() {
        if(mMovies!=null){
            return mMovies.size();
        }
        return 0;
    }

    public void setmSlides(List<Movie> mMovies) {
        this.mMovies = mMovies;
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
