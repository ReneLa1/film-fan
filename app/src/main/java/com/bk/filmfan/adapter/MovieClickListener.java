package com.bk.filmfan.adapter;

import android.widget.ImageView;

import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;

public interface MovieClickListener {

    //an imageview is needed to make shared animation
   void onMovieClick(Movie movie, ImageView movieImageView);
   void onSlideClick(Movie movie, ImageView movieImageView);
}
