package com.bk.filmfan.response;

import com.bk.filmfan.models.Cast;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieCastsResponse {
    @SerializedName("cast")
    @Expose
    private List<Cast> casts;

    public MovieCastsResponse(List<Cast> casts) {
        this.casts = casts;
    }

    public List<Cast> getCasts() {
        return casts;
    }

    public void setCasts(List<Cast> casts) {
        this.casts = casts;
    }
}
