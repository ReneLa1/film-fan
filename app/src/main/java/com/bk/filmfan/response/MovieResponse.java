package com.bk.filmfan.response;

import com.bk.filmfan.models.Movie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//this class is for requesting a single movie
public class MovieResponse {

    // 1- finding movie object(we use serializing object)
    @SerializedName("results")
    @Expose
    private Movie movie;

    public Movie getMovie(){
        return movie;
    }

    @Override
    public String toString() {
        return "MovieResponse{" +
                "movie=" + movie +
                '}';
    }
}
