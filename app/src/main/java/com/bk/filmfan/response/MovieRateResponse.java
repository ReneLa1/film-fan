package com.bk.filmfan.response;

import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.MovieRate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MovieRateResponse {

    @SerializedName("status_success")
    @Expose
    private MovieRate movieRate;

    public MovieRateResponse(MovieRate movieRate) {
        this.movieRate = movieRate;
    }

    public MovieRate getMovieRate() {
        return movieRate;
    }

    @Override
    public String  toString() {
        return "MovieRateResponse{" +
                "movieRate=" + movieRate +
                '}';
    }
}
