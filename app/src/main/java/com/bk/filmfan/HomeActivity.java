package com.bk.filmfan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import com.bk.filmfan.adapter.CastAdapter;
import com.bk.filmfan.adapter.MovieAdapter;
import com.bk.filmfan.adapter.MovieClickListener;
import com.bk.filmfan.adapter.SliderPagerAdapter;
import com.bk.filmfan.helpers.Helpers;
import com.bk.filmfan.models.Cast;
import com.bk.filmfan.models.Movie;
import com.bk.filmfan.models.Slide;
import com.bk.filmfan.request.CallService;
import com.bk.filmfan.request.MovieApi;
import com.bk.filmfan.response.MovieCastsResponse;
import com.bk.filmfan.response.MovieResponse;
import com.bk.filmfan.response.NowPlayingResponse;
import com.bk.filmfan.viewmodels.MovieListViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements MovieClickListener {

    // View model
    private MovieListViewModel movieListViewModel;

    private RecyclerView moviesRV;
    private MovieAdapter movieRecyclerAdapter;

    private ViewPager sliderpager;
    private SliderPagerAdapter sliderPagerAdapter;
    private List<Slide> lstSlides;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sliderpager = findViewById(R.id.poster_slider);
        moviesRV = findViewById(R.id.now_playing_rv);
        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        ConfigureRecyclerView();
        ConfigureSliderView();

        //calling observers
        ObserveNowPlayingChange();
//        ObserveSlideDataChange();

        getNowPlayingMoviesApi(1);

    }

    //Observing for now playing movies
    private void ObserveNowPlayingChange() {
        movieListViewModel.getMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                //Observing for any data change
                Collections.sort(movies, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie_1, Movie movie_2) {
                        return movie_1.getTitle().compareTo(movie_2.getTitle());
                    }
                });
                if (movies != null) {
                    for (Movie movie : movies) {
                        //Get data in the log
//                        Log.v("Tag", "The title " + movie.getTitle());
                        movieRecyclerAdapter.setmMovies((movies));
                        sliderPagerAdapter.setmSlides((movies));
                    }
                }
            }
        });
    }


    @Override
    public void onMovieClick(Movie movie, ImageView movieImageView) {

        Intent intent = new Intent(this, MovieDetailActivity.class);

        //send movie details to details screen
        intent.putExtra("movie", movie);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(HomeActivity.this,
                movieImageView, "sharedMovie");
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onSlideClick(Movie movie, ImageView movieImageView) {
//        Log.v("Tag", "Clicked");

        Intent intent = new Intent(this, MovieDetailActivity.class);

        //send movie details to details screen
        intent.putExtra("movie", movie);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(HomeActivity.this,
                movieImageView, "sharedMovie");
        startActivity(intent, options.toBundle());
    }

    //4- calling get now playing method in home activity
    private void getNowPlayingMoviesApi(int pageNumber) {
        movieListViewModel.getNowPlayingMoviesApi(pageNumber);
    }


    //initializing recyclerview and adding data to it
    public void ConfigureRecyclerView() {
        movieRecyclerAdapter = new MovieAdapter(this);
        moviesRV.setAdapter(movieRecyclerAdapter);
        moviesRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    //configuring the slider
    public void ConfigureSliderView(){
        sliderPagerAdapter= new SliderPagerAdapter(this);
        sliderpager.setAdapter(sliderPagerAdapter);
    }
}