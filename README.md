# Film-Fan

Film fan is a movie streaming android mobile app

**Technologies**
- Android/Java
- Retrofit2 for http calls

- https://www.themoviedb.org/ for apis

**Screens**
- Splash screen 
- Welcome screen
- Login screen(with no credentials required-- still in progress)
- Home movie screen showcasing nowplaying movies 
- Movie details screen showcasing movie details, casts of the movie and a rate movie functionality

**How to Install the app**
- Must have android studio installed
- A physical android device or emulator configured in you pc 
- Clone the main repository `git clone git@gitlab.com:ReneLa1/film-fan.git`
- Run app from android studio and test on testing device 
    OR
- Download apk and Install to physical device: https://drive.google.com/file/d/1ZEweUq1mK6aW9lCg1-r2SU5xkkVtRFqK/view?usp=sharing    



